package com.footballskills;

import android.os.Bundle;
import com.swarmconnect.Swarm;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class MainActivity extends AndroidApplication {
    
	private static final int MY_APP_D = 4903;
	private static final String MY_APP_K = "6f34759d399918e38c4dbe3bb66a48ac";
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useGL20 = false;
        Swarm.setActive(this);
        initialize(new FootballSkills(), cfg);
        
    }

    public void onResume()
	{
		super.onResume();
		Swarm.setActive(this);
	    Swarm.init(this, MY_APP_D, MY_APP_K);

	}
	public void onPause()
	{
	    super.onPause();
	    Swarm.setInactive(this);
	}
	public void onStop()
	{
	    super.onStop();
	    Swarm.setInactive(this);
	}
	public void onDestroy()
	{
	    super.onDestroy();
	    Swarm.setInactive(this);
	}
    
}