package com.footballskills;

import com.badlogic.gdx.Game;

public class FootballSkills extends Game {
	
	private IActivityRequestHandler myRequestHandler;
	
	@Override
	public void create() {		
		Assets.load();
		setScreen(new MainMenuScreen(this, myRequestHandler));	
	}
}
