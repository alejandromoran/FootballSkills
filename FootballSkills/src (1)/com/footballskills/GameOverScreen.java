package com.footballskills;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class GameOverScreen implements Screen {

	private Game game;
	private Stage stage;
	private Label lbl;
	private TextButton playAgain, mainMenu, exitGame;
	private IActivityRequestHandler myRequestHandler;
	private String teamName;
	private int score;
	
	public GameOverScreen (Game game,String teamName, IActivityRequestHandler handler, int score)
	{
		this.game = game;
		this.score = score;
		this.teamName = teamName;
		//myRequestHandler = handler;
		//myRequestHandler.showAds(true);
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		stage = new Stage();
		playAgain = new TextButton("Play again", Assets.skin);
		mainMenu = new TextButton("Menu", Assets.skin);

		Table table = new Table(Assets.skin);
		Gdx.input.setInputProcessor(stage);
		
		playAgain.addListener(new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				// TODO Auto-generated method stub
				clearScreen();
				game.setScreen(new GameScreen(game,teamName, myRequestHandler));
				return true;
			}

		});

		mainMenu.addListener(new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				clearScreen();
				game.setScreen(new MainMenuScreen(game, myRequestHandler));
				return true;
			}

		});
		

		table.setFillParent(true);
		String lblText = "You've got " + score + " points";
		lbl = new Label(lblText, Assets.skin);
		
		table.add(lbl).padBottom(15);
		table.row();
		table.add(playAgain).width(150).height(50).padBottom(1);
		table.row();
		table.add(mainMenu).width(150).height(50).padBottom(1);
		table.row();
		table.add(exitGame).width(150).height(50).padBottom(1);

		stage.addActor(table);
	}
	
	private void clearScreen()
	{
		stage.clear();
	}
	
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
