package com.footballskills;

import java.util.Iterator;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class GameScreen implements Screen {

	private Game game;
	private Array<Enemy> enemies;
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Circle player;
	private long lastDropTime, spawnTime;
	private float backgroundXPosition = 0;
	private boolean changeDirection = false;
	private Label lbl;
	private Stage stage;
	private Table table;
	private int score, screenWidth, screenHeight;
	private IActivityRequestHandler myRequestHandler;
	private int enemyNumber;
	private String teamName;
	// It's 64/480 because is the correct resolution I started to work with
	private final double widthRatio = 0.13; 
	
	private int playerWidth;
	
	public GameScreen(Game game,String teamName, IActivityRequestHandler handler)
	{
		this.game = game;
		this.teamName = teamName;
		score = enemyNumber =0;
		spawnTime = 425000000;
		//myRequestHandler = handler;
		//myRequestHandler.showAds(false);
		screenWidth = Assets.getScreenWidth();
		screenHeight = Assets.getScreenHeight();
		enemies = new Array<Enemy>();
		camera = new OrthographicCamera();
		camera.setToOrtho(false,screenWidth,screenHeight);
		batch = new SpriteBatch();
		playerWidth = getWidthWithRatio();
		player = new Circle((screenWidth/2)-playerWidth, 0, playerWidth/2);
		SoundManager.playMusic();

		Assets.loadTeamTextures(teamName);
	}
	
	private int getWidthWithRatio() {
		return (int) (widthRatio * screenWidth);
	}

	private void spawnEnemy()
	{
		Enemy enemy = new Enemy(MathUtils.random(0, screenWidth-playerWidth),screenHeight,playerWidth/2);
		enemy.setId(enemyNumber);
		
		enemy.setSpeed(MathUtils.random(200, 400));	
		
		enemies.add(enemy);
		lastDropTime = TimeUtils.nanoTime();
	}
	
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();	
		batch.draw(Assets.gameScreenBackground,backgroundXPosition,0,screenWidth + (screenHeight/2),screenHeight);
		batch.draw(Assets.player, player.x, player.y, playerWidth, playerWidth);
		batch.end();
		
		if(Gdx.input.isTouched()) {
			Vector3 touchPos = new Vector3();
			touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			camera.unproject(touchPos);
			player.x = touchPos.x - (playerWidth/2);
			player.y = touchPos.y - (playerWidth/2);
			
		}

		backgroundControl();
		
		if(TimeUtils.nanoTime() - lastDropTime > spawnTime)
		{
			spawnEnemy();
		}
		
		
		Iterator<Enemy> iter = enemies.iterator();
		while(iter.hasNext())
		{		
			Enemy enemy = iter.next();
			enemy.y -=  (enemy.getSpeed() * Gdx.graphics.getDeltaTime()) + (int) enemyNumber/2;
			if(enemy.y + 48 < 0)
			{
				score++;

				spawnTime = spawnTime - 750000;

				
				iter.remove();
			}
			if (Intersector.overlapCircles(enemy, player))
			{
				dispose();
				game.setScreen(new GameOverScreen(game,teamName, myRequestHandler, score));
				return;
			}
		}
		
		
		
		batch.begin();
		
		
		for(Enemy enemy: enemies) {
			batch.draw(Assets.getEnemy(enemy.getIdentifier()), enemy.x, enemy.y,playerWidth,playerWidth);
		}
		batch.end();
		stage.act(delta);
		stage.draw();
		
		String lblText = "Score: " + score;
		lbl = new Label(lblText, Assets.skin);
		table.clear();
		table.add(lbl).bottom().left();
		table.setX(lbl.getWidth()/2 + 10);
		table.setY(screenHeight - 15);
		stage.addActor(table);
	}			

	private void backgroundControl()
	{
		int bgAssetWidth = screenWidth + (screenHeight/2); 
		if((backgroundXPosition+bgAssetWidth )> screenWidth && !changeDirection)
		{
			backgroundXPosition -= 0.5;
		}
		if ( (backgroundXPosition+bgAssetWidth) <= screenWidth && !changeDirection)
		{
			changeDirection = true;
		}

		if (backgroundXPosition< 0 && changeDirection)
		{
			backgroundXPosition += 0.5;
		}
		
		if (backgroundXPosition >= 0 && changeDirection)
		{
			changeDirection = false;
		}
	}
	
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		stage = new Stage();
		table = new Table(Assets.skin);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		SoundManager.stopMusic();
		stage.clear();
		stage.dispose();
		batch.dispose();
	}

}
