package com.footballskills;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class Assets {

	public static Texture mainMenuScreen, player, enemy, enemy2,enemy3,enemy4,enemy5,enemy6;
	public static Music bso;
	public static Texture mainMenuScreenBackground, gameScreenBackground, gameOverScreenBackground;
	public static Skin skin;
	public static ArrayList<String> supportedScreens;
	public static int screenWidth, screenHeight;
	public static String screenResolution, screenPath;
	public static Preferences pref;
	
	private static List<Texture> enemies;
	
	public static void load()
	{
		pref = Gdx.app.getPreferences(".userData");
		supportedScreens = new ArrayList<String>();
		supportedScreens.add("480x800");
		supportedScreens.add("800x1280");
		supportedScreens.add("720x1280");
		supportedScreens.add("480x854");
		supportedScreens.add("240x432");
		supportedScreens.add("240x400");
		supportedScreens.add("320x480");
		supportedScreens.add("240x320");
		supportedScreens.add("600x1024");
		supportedScreens.add("540x960");
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();	
		screenResolution = screenWidth + "x" + screenHeight;	
		mainMenuScreenBackground = new Texture(Gdx.files.internal("data/screens/MainMenu.png"));
		gameScreenBackground = new Texture(Gdx.files.internal("data/screens/Background.png"));
		//player = new Texture(Gdx.files.internal("data/actors/player.png"));
		player = new Texture(Gdx.files.internal("data/actors/ball.png"));
		
		
		skin = new Skin(Gdx.files.internal("data/uiskin.json"));
		loadMusic();
	}
	
	public static void loadTeamTextures(String teamName)
	{
		enemies = new ArrayList<Texture>();
		//File directory = new File("data/actors/" + teamName);	
		//Gdx.app.error("ERRORCO�O", directory.g + " " + directory.getPath() );
		for(int i=1;i<=14;i++)
			enemies.add(new Texture(Gdx.files.internal("data/actors/" + teamName + "/"+ i +".png")));
	}
	
	public static Texture getEnemy(int identifier)
	{
		return enemies.get(identifier);
	}
	
	public static void loadMusic()
	{
		bso = Gdx.audio.newMusic(Gdx.files.internal("data/bso.mp3"));
	}
	
	public static void preferencesPutString(String key, String val)
	{	
		pref.putString(key, val);
		pref.flush();
	}
	
	public static void switchMusicControl()
	{	
		pref.putBoolean("musicStatus", !getLastMusicStatus());
		pref.flush();
	}
	
	public static String getResolution()
	{
		return screenResolution;
	}
	
	public static boolean getLastMusicStatus()
	{
		return pref.getBoolean("musicStatus", true);
	}
	
	public static int getScreenWidth()
	{
		return screenWidth;
	}
	
	public static int getScreenHeight()
	{
		return screenHeight;
	}
	
	public static String getLanguage()
	{
		return Locale.getDefault().getLanguage();
	}
	
}
