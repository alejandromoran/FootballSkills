package com.footballskills;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;

public class Enemy extends Circle {

	private static final long serialVersionUID = 4452941336424155849L;
	private int id;
	private float speed;
	private int identifier;
	
	public Enemy(float x, float y, float radius)
	{
		super(x,y,radius);
		identifier = MathUtils.random(0,13);
	}
	public int getIdentifier()
	{
		return identifier;
	}
	
	public float getSpeed()
	{
		return speed;
	}
	
	public int getId()
	{
		return id;
	}
	
	public void setSpeed(float speed)
	{
		this.speed = speed;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
}
